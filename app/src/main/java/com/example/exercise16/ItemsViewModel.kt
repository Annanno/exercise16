package com.example.exercise16

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exercise16.database.AppDatabase
import com.example.exercise16.entity.Item
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class ItemsViewModel: ViewModel() {

    val items: Flow<List<Item>> = AppDatabase.db.itemDao().getAll()

    fun addItem(item: Item){
        viewModelScope.launch {
            AppDatabase.db.itemDao().addItem(item)
        }

    }

    fun updateItem(item: Item){
        viewModelScope.launch {
            AppDatabase.db.itemDao().updateItem(item)
        }
    }

    fun deleteItem(item: Item){
        viewModelScope.launch {
            AppDatabase.db.itemDao().deleteItem(item)
        }
    }

}