package com.example.exercise16.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.exercise16.databinding.ItemLayoutBinding
import com.example.exercise16.entity.Item
import com.example.exercise16.extensions.setImage
import com.example.exercise16.fragments.ItemsListFragmentDirections

typealias ClickItem = (item: Item) -> Unit

class ItemsAdapter: RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {


    private val items = mutableListOf<Item>()
    var callback: ClickItem? = null



    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnLongClickListener{
//        private lateinit var currentItem: Item
        fun onBind() {
            val currentItem = items[adapterPosition]
            binding.titleTv.text = currentItem.title
            binding.description.text = currentItem.description

            binding.imageView.setImage(currentItem.image)

            binding.backGround.setOnLongClickListener(this)

            binding.editBtn.setOnClickListener {
                it.findNavController().navigate(ItemsListFragmentDirections.actionItemsListFragmentToUpdateFragment(currentItem))
            }

        }

        override fun onLongClick(p0: View?): Boolean {
            callback?.invoke(items[adapterPosition])
            return true
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }

    override fun getItemCount() = items.size

    fun setData(items: MutableList<Item>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()

    }
}
