package com.example.exercise16.dao

import androidx.room.*
import com.example.exercise16.entity.Item
import kotlinx.coroutines.flow.Flow


@Dao
interface ItemDao {
    @Query("SELECT * FROM items_table")
    fun getAll(): Flow<List<Item>>

    @Insert
    suspend fun addItem(item: Item)

    @Update
    suspend fun updateItem(item: Item)

    @Delete
    suspend fun deleteItem(item: Item)

}
