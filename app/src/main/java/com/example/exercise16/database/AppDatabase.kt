package com.example.exercise16.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.exercise16.App
import com.example.exercise16.dao.ItemDao
import com.example.exercise16.entity.Item

@Database(entities = [Item::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao

    companion object{
        val db by lazy {
            Room.databaseBuilder(
                App.appContext!!,
                AppDatabase::class.java, "users_database"
            ).fallbackToDestructiveMigration()
                .build()
        }
    }
}