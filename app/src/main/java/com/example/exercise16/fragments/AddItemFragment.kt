package com.example.exercise16.fragments

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.exercise16.ItemsViewModel
import com.example.exercise16.base.BaseFragment
import com.example.exercise16.databinding.FragmentAddItemBinding
import com.example.exercise16.entity.Item


class AddItemFragment : BaseFragment<FragmentAddItemBinding>(FragmentAddItemBinding::inflate) {

    private val viewModel: ItemsViewModel by viewModels()

    override fun init() {
        setListener()

    }

    private fun setListener(){
        binding.Add.setOnClickListener {
            addItemToDatabase()
            findNavController().navigate(AddItemFragmentDirections.actionAddItemFragmentToItemsListFragment())
        }
    }


    private fun addItemToDatabase(){
        val title = binding.titleEt.text.toString()
        val description = binding.descriptionEt.text.toString()
        val imageUrl = binding.imageUrlEt.text.toString()

        val item = Item(0, description, imageUrl, title)

        viewModel.addItem(item)

    }

}