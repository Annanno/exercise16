package com.example.exercise16.fragments


import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exercise16.ItemsViewModel
import com.example.exercise16.adapter.ItemsAdapter
import com.example.exercise16.base.BaseFragment
import com.example.exercise16.databinding.FragmentItemsListBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class ItemsListFragment : BaseFragment<FragmentItemsListBinding>(FragmentItemsListBinding::inflate) {

    private val viewModel: ItemsViewModel by viewModels()
    private lateinit var itemsRecycler: ItemsAdapter

    override fun init() {
        initRecycler()
        observeDatabase()
        setListener()
    }

    private fun setListener(){
        binding.addBtn.setOnClickListener{
            findNavController().navigate(ItemsListFragmentDirections.actionItemsListFragmentToAddItemFragment())
        }
    }

    private fun observeDatabase(){
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.items.collect {
                    itemsRecycler.setData(it.toMutableList())
                }
            }
        }

    }

    private fun initRecycler(){
        itemsRecycler = ItemsAdapter()
        binding.itemsRecycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter =itemsRecycler
        }
        itemsRecycler.callback = {
            viewModel.deleteItem(it)
        }
    }

}