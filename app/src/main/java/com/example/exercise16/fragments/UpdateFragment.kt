package com.example.exercise16.fragments


import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.exercise16.ItemsViewModel
import com.example.exercise16.base.BaseFragment
import com.example.exercise16.databinding.FragmentUpdateBinding
import com.example.exercise16.entity.Item


class UpdateFragment : BaseFragment<FragmentUpdateBinding>(FragmentUpdateBinding::inflate) {

    private val args by navArgs<UpdateFragmentArgs>()
    private val viewModel: ItemsViewModel by viewModels()



    override fun init() {
        setData()
        setListener()


    }

    private fun setListener(){

        binding.update.setOnClickListener {
            updateItem()

        }
    }

    private fun setData(){
        binding.titleEt.setText(args.currentItem.title)
        binding.descriptionEt.setText(args.currentItem.description)
        binding.imageUrlEt.setText(args.currentItem.image)
    }

    private fun updateItem(){
        val title = binding.titleEt.text.toString()
        val description = binding.descriptionEt.text.toString()
        val imageUrl = binding.imageUrlEt.text.toString()

        val item = Item(args.currentItem.uid, description, imageUrl, title)

        viewModel.updateItem(item)
        findNavController().navigate(UpdateFragmentDirections.actionUpdateFragmentToItemsListFragment())

    }

}